#include <stdio.h>
#include <psp2/io/fcntl.h>
#include "FormattedStringException.h"
#include "SfoFile.h"

using namespace std;

SfoFile::SfoFile(string path)
{
	SceUID file = sceIoOpen(path.c_str(), SCE_O_RDONLY, 0777);
	if (file < 0)
	{
		throw FormattedStringException("Couldn't open SFO file (0x%08x)", file);
	}
	int size = sceIoLseek(file, 0, SCE_SEEK_END);
	sceIoLseek(file, 0, SCE_SEEK_SET);

	uint8_t* buffer = new uint8_t[size];
	int ret = sceIoRead(file, buffer, size);
	if (ret != size)
	{
		delete[] buffer;
		throw FormattedStringException("Read bytes count differ from expected bytes");
	}
	ret = sceIoClose(file);
	if (ret < 0)
	{
		delete[] buffer;
		throw FormattedStringException("Couldn't close SFO file (0x%08x)", ret);
	}

	SfoHeader header = *(SfoHeader*)buffer;
	if (header.magic != SFO_MAGIC)
	{
		throw FormattedStringException("Wrong header in SFO file (0x%08x)", header.magic);
	}

	for (uint i = 0; i < header.count; i++)
	{
		SfoEntry entryInfo = *(SfoEntry*)(buffer + 20 + i * 16);
		SfoKVEntry entry;
		entry.type = entryInfo.type;
		entry.name = "";
		int j = 0;
		while (true)
		{
			char chr = buffer[header.keyofs + entryInfo.keyofs + j];
			if (chr == '\0')
			{
				break;
			}
			entry.name += chr;
			j++;
		}
		entry.value.clear();
		for (int j = 0; j < entryInfo.totalsize; j++)
		{
			entry.value.push_back(buffer[header.valofs + entryInfo.dataofs + j]);
		}
		entries.push_back(entry);
	}

	delete[] buffer;
}

string SfoFile::GetKeyString(string name)
{
	for (int i = 0; i < entries.size(); i++)
	{
		if (entries[i].name == name)
			return string(entries[i].value.data());
	}
	throw FormattedStringException("Key not found in SFO file: %s", name.c_str());
}

uint SfoFile::GetKeyUInt(string name)
{
	for (int i = 0; i < entries.size(); i++)
	{
		if (entries[i].name == name)
			return *(uint*)(entries[i].value.data());
	}
	throw FormattedStringException("Key not found in SFO file: %s", name.c_str());
}

void SfoFile::SetKeyString(string name, string value)
{
	for (int i = 0; i < entries.size(); i++)
	{
		if (entries[i].name == name)
		{
			entries[i].type = PSF_TYPE_STR;
			entries[i].value.clear();
			for (int j = 0; j < value.size(); j++)
			{
				entries[i].value.push_back(value[j]);
			}
			entries[i].value.push_back('\0');
		}
	}
}

void SfoFile::SetKeyUInt(string name, uint value)
{
	for (int i = 0; i < entries.size(); i++)
	{
		if (entries[i].name == name)
		{
			entries[i].type = PSF_TYPE_VAL;
			entries[i].value.clear();
			for (int j = 0; j < 4; j++)
			{
				entries[i].value.push_back(((char*)&value)[j]);
			}
		}
	}
}

void SfoFile::Write(string path)
{
	int file = sceIoOpen(path.c_str(), SCE_O_WRONLY | SCE_O_TRUNC | SCE_O_CREAT, 0777);
	if (file < 0)
	{
		throw FormattedStringException("Couldn't open or create SFO file (0x%08x)", file);
	}

	sceIoLseek(file, 0, SCE_SEEK_SET);
	SfoHeader header;
	header.magic = SFO_MAGIC;
	header.version = 0x00000101;
	header.keyofs = 20 + entries.size() * 16;
	header.valofs = 0;
	header.count = entries.size();
	int ret = sceIoWrite(file, &header, sizeof(SfoHeader));
	if (ret != sizeof(SfoHeader))
	{
		sceIoClose(file);
		throw FormattedStringException("Written bytes count differ from expected count");
	}

	uint16_t keyOffset = 0;
	uint32_t valOffset = 0;
	for (int i = 0; i < entries.size(); i++)
	{
		SfoEntry entryInfo;
		entryInfo.keyofs = keyOffset;
		entryInfo.alignment = 4;
		entryInfo.type = entries[i].type;
		entryInfo.valsize = entries[i].value.size();
		if (entryInfo.type == PSF_TYPE_STR)
			entryInfo.valsize--;
		entryInfo.totalsize = entries[i].value.size();
		entryInfo.dataofs = valOffset;
		ret = sceIoWrite(file, &entryInfo, sizeof(SfoEntry));
		if (ret != sizeof(SfoEntry))
		{
			sceIoClose(file);
			throw FormattedStringException("Written bytes count differ from expected count");
		}

		keyOffset += entries[i].name.size() + 1;
		valOffset += entryInfo.totalsize;
	}
	for (int i = 0; i < entries.size(); i++)
	{
		ret = sceIoWrite(file, entries[i].name.c_str(), entries[i].name.size() + 1);
		if (ret != entries[i].name.size() + 1)
		{
			sceIoClose(file);
			throw FormattedStringException("Written bytes count differ from expected count");
		}
	}
	uint8_t zero = 0;
	while (sceIoLseek(file, 0, SCE_SEEK_CUR) % 4 != 0)
	{
		ret = sceIoWrite(file, &zero, 1);
		if (ret != 1)
		{
			sceIoClose(file);
			throw FormattedStringException("Written bytes count differ from expected count");
		}
		keyOffset++;
	}
	for (int i = 0; i < entries.size(); i++)
	{
		ret = sceIoWrite(file, entries[i].value.data(), entries[i].value.size());
		if (ret != entries[i].value.size())
		{
			sceIoClose(file);
			throw FormattedStringException("Written bytes count differ from expected count");
		}
	}
	sceIoLseek(file, 12, SCE_SEEK_SET);
	header.valofs = header.keyofs + keyOffset;
	ret = sceIoWrite(file, &header.valofs, 4);
	if (ret != 4)
	{
		sceIoClose(file);
		throw FormattedStringException("Written bytes count differ from expected count");
	}

	ret = sceIoClose(file);
	if (ret < 0)
	{
		throw FormattedStringException("Couldn't close SFO file (0x%08x)", ret);
	}
}

SfoFile::~SfoFile()
{

}
