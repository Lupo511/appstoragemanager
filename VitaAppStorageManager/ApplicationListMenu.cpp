#include "ApplicationListMenu.h"
#include "FormattedStringException.h"
#include "savedata_dialog.h"
#include "graphics.h"

#include <psp2/io/fcntl.h>
#include <psp2/kernel/sysmem.h>
#include <psp2/gxm.h>
#include <psp2/display.h>

int getFileSize(const char* path)
{
	SceUID file = sceIoOpen(path, SCE_O_RDONLY, 0777);
	if (file < 0)
	{
		return file;
	}
	int size = sceIoLseek(file, 0, SCE_SEEK_END);
	sceIoClose(file);
	return size;
}

static void *g_alloc(SceKernelMemBlockType type, SceUInt32 size, SceUInt32 alignment, SceUInt32 attribs, SceUID *uid)
{
	void	*mem = NULL;
	int		res;

	// CDRAM memblocks must be 256KiB aligned
	if (type == SCE_KERNEL_MEMBLOCK_TYPE_USER_CDRAM_RW) {
		if (alignment > 0x40000)
			return NULL;
		size = (size + 0x3ffffU) & ~0x3ffffU;
	}
	// LPDDR memblocks must be 4KiB aligned
	else {
		if (alignment > 0x1000)
			return NULL;
		size = (size + 0xfffU) & ~0xfffU;
	}

	res = sceKernelAllocMemBlock("test block", type, size, NULL);
	if (res < 0)
		return NULL;
	*uid = res;

	res = sceKernelGetMemBlockBase(*uid, &mem);
	if (res != 0)
		return NULL;
	res = sceGxmMapMemory(mem, size, (SceGxmMemoryAttribFlags)attribs);
	if (res != 0)
		return NULL;

	return mem;
}

static inline void sceCommonDialogConfigParamInitCustom(SceCommonDialogConfigParam *param)
{
	memset(param, 0x0, sizeof(SceCommonDialogConfigParam));
	param->language = 0U;
	param->enterButtonAssign = 0U;
	param->sdkVersion = 0x03150021;
};

ApplicationListMenu::ApplicationListMenu()
{
	psvDebugScreenPrintf("Initializing list menu\n");

	SceAppUtilInitParam appUtilParam;
	memset(&appUtilParam, 0, sizeof(SceAppUtilInitParam));
	SceAppUtilBootParam bootParam;
	memset(&bootParam, 0, sizeof(SceAppUtilBootParam));
	int ret = sceAppUtilInit(&appUtilParam, &bootParam);
	if (ret < 0)
	{
		throw FormattedStringException("Error in app util init (0x%08x)", ret);
		return;
	}

	SceCommonDialogConfigParam commonParam;
	sceCommonDialogConfigParamInitCustom(&commonParam);
	ret = sceCommonDialogSetConfigParam(&commonParam);
	if (ret < 0)
	{
		throw FormattedStringException("Error in common param set init (0x%08x)", ret);
		return;
	}

	SceSaveDataDialogParam param;
	sceSaveDataDialogParamInit(&param);

	SceCommonDialogColor black;
	black.r = 0;
	black.g = 0;
	black.b = 0;
	black.a = 255;
	param.commonParam.bgColor = &black;
	param.commonParam.dimmerColor = &black;
	SceCommonDialogInfobarParam infoParam;
	memset(&infoParam, 0, sizeof(SceCommonDialogInfobarParam));
	param.commonParam.infobarParam = &infoParam;

	param.mode = SCE_SAVEDATA_DIALOG_MODE_LIST;
	param.dispType = SCE_SAVEDATA_DIALOG_TYPE_SAVE;

	SceSaveDataDialogListParam listParam;
	memset(&listParam, 0, sizeof(SceSaveDataDialogListParam));

	SceAppUtilSaveDataSlot slot;
	memset(&slot, 0, sizeof(SceAppUtilSaveDataSlot));
	slot.id = 0;
	slot.status = 0;
	slot.userParam = 0;

	SceAppUtilSaveDataSlotEmptyParam emptyParam;
	memset(&emptyParam, 0, sizeof(SceAppUtilSaveDataSlotEmptyParam));
	emptyParam.title = (SceWChar16*)L"Test Slot";
	emptyParam.iconPath = "app0:sce_sys/livearea/contents/default_gate.png";
	emptyParam.iconBuf = NULL;
	emptyParam.iconBufSize = 0;
	slot.emptyParam = &emptyParam;

	listParam.slotList = &slot;
	listParam.slotListSize = 1;
	listParam.focusPos = SCE_SAVEDATA_DIALOG_FOCUS_POS_LISTHEAD;
	listParam.focusId = 0;
	listParam.listTitle = (SceChar8*)"Test List";

	param.listParam = &listParam;
	param.flag = SCE_SAVEDATA_DIALOG_ENV_FLAG_DEFAULT;

	psvDebugScreenPrintf("Initializing dialog\n");
	ret = sceSaveDataDialogInit(&param);
	if (ret < 0)
	{
		SceSaveDataDialogResult result;
		int ret2 = sceSaveDataDialogGetResult(&result);
		throw FormattedStringException("Error in dialog init (0x%08x) (0x%08x) (%i)", ret, ret2, result.result);
		return;
	}
	psvDebugScreenPrintf("Dialog init completed\n");

	while (true) {}

	/*SceGxmSyncObject* syncObj;
	sceGxmSyncObjectCreate(&syncObj);
	psvDebugScreenPrintf("Sync object created\n");

	SceUID bufUID;
	void* buffer = g_alloc(SCE_KERNEL_MEMBLOCK_TYPE_USER_CDRAM_RW,
		((4 * 1024 * 544 + 0xfffffU) & ~0xfffffU),
		SCE_GXM_COLOR_SURFACE_ALIGNMENT,
		SCE_GXM_MEMORY_ATTRIB_READ | SCE_GXM_MEMORY_ATTRIB_WRITE,
		&bufUID);
	psvDebugScreenPrintf("Buffer created\n");

	SceUID dsUID;
	void* ds = g_alloc(SCE_KERNEL_MEMBLOCK_TYPE_USER_CDRAM_RW,
		((4 * 1024 * 544 + 0xfffffU) & ~0xfffffU),
		SCE_GXM_DEPTHSTENCIL_SURFACE_ALIGNMENT,
		SCE_GXM_MEMORY_ATTRIB_READ | SCE_GXM_MEMORY_ATTRIB_WRITE,
		&dsUID);
	psvDebugScreenPrintf("Depth Stencil created\n");

	SceDisplayFrameBuf buf;
	buf.base = buffer;
	buf.width = 960;
	buf.height = 544;
	buf.pitch = 1024;
	buf.size = ((4 * 1024 * 544 + 0xfffffU) & ~0xfffffU);
	buf.pixelformat = SCE_DISPLAY_PIXELFORMAT_A8B8G8R8;
	psvDebugScreenPrintf("Buf initialized\n");

	while (true)
	{
		SceCommonDialogUpdateParam	updateParam;

		memset(&updateParam, 0, sizeof(updateParam));
		updateParam.renderTarget.colorFormat = SCE_GXM_COLOR_FORMAT_A8B8G8R8;
		updateParam.renderTarget.surfaceType = SCE_GXM_COLOR_SURFACE_LINEAR;
		updateParam.renderTarget.width = 960;
		updateParam.renderTarget.height = 544;
		updateParam.renderTarget.strideInPixels = 1024;

		updateParam.renderTarget.colorSurfaceData = buffer;
		updateParam.renderTarget.depthSurfaceData = ds;
		updateParam.displaySyncObject = syncObj;

		sceCommonDialogUpdate(&updateParam);

		sceDisplaySetFrameBuf(0, SCE_DISPLAY_SETBUF_NEXTFRAME);
	}*/
}


ApplicationListMenu::~ApplicationListMenu()
{
}
