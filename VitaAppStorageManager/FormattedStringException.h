#pragma once
#include <string>

using namespace std;

class FormattedStringException : public exception
{
private:
	std::string msg;
public:
	FormattedStringException(const char* format, ...) throw();
	virtual const char* what() const throw();
	~FormattedStringException() throw();
};

