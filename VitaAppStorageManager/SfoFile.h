#pragma once
#include <string>
#include <vector>

using namespace std;

#define SFO_MAGIC 0x46535000

#define PSF_TYPE_BIN 0
#define PSF_TYPE_STR 2
#define PSF_TYPE_VAL 4

typedef struct {
	uint32_t magic;
	uint32_t version;
	uint32_t keyofs;
	uint32_t valofs;
	uint32_t count;
} SfoHeader;

typedef struct {
	uint16_t keyofs;
	uint8_t  alignment;
	uint8_t  type;
	uint32_t valsize;
	uint32_t totalsize;
	uint32_t dataofs;
} SfoEntry;

typedef struct {
	uint8_t type;
	string name;
	vector<char> value;
} SfoKVEntry;

class SfoFile
{
public:
	vector<SfoKVEntry> entries;
public:
	SfoFile(string);
	string GetKeyString(string name);
	uint GetKeyUInt(string name);
	void SetKeyString(string name, string value);
	void SetKeyUInt(string name, uint value);
	void Write(string path);
	~SfoFile();
};