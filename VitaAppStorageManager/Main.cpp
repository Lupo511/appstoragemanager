#include <stdlib.h>
#include <stdio.h>
#include <cstring>
#include <string>
#include <vector>
#include <psp2\kernel\processmgr.h>
#include <psp2\ctrl.h>
#include <psp2\appmgr.h>
#include <psp2\io\fcntl.h>
#include <psp2\io\dirent.h>
#include <psp2\io\devctl.h>

#include "graphics.h"
#include "sqlite3\sqlite3.h"
#include "AppInfo.h"
#include "SfoFile.h"
#include "ApplicationListMenu.h"

#define SCE_ERROR_ERRNO_EEXIST 0x80010011

typedef struct {
	uint64_t max_size;
	uint64_t free_size;
	uint32_t cluster_size;
	void *unk;
} SceIoDevInfo;

extern "C"
int sceRegMgrGetKeyInt(const char*, const char*, int*);

#define TRANSFER_SIZE 64 * 1024

using namespace std;

SceCtrlData lastData;
int confirmButton = 0;
int cancelButton = 0;
int currentScreen = 0;
int transferMode = 0;
int selectedApp = 0;
vector<AppInfo> currentApps;

void redrawMenu(bool clear);
void scanApps(bool internalStorage);
void findCurrentAppsInAppDb();
int sqlAddAppCallback(void* data, int argc, char** argv, char** cols);
int sqlSetTitleCallback(void* data, int argc, char** argv, char** cols);
void moveApp(bool toInternal, AppInfo app);
int getFolderSize(string path);
int removeFolder(string path);
int copyFolder(string source, string dest);
int copyFile(string source, string dest);
bool waitConfirm();
void getConfirmButton();

int main()
{
	int ret = 0;

	psvDebugScreenInit();
	try
	{
		ApplicationListMenu menu;
	}
	catch (exception& e)
	{
		psvDebugScreenPrintf(e.what());
		while (true) {}
	}

	getConfirmButton();

	ret = sceCtrlSetSamplingMode(SCE_CTRL_MODE_ANALOG);
	if (ret < 0)
	{
		psvDebugScreenSetFgColor(COLOR_RED);
		psvDebugScreenPrintf("Error during initialization: cannot set ctrl sampling mode (0x%08x)", ret);
		while (true) {}
	}

	ret = sceCtrlPeekBufferPositive(0, &lastData, 1);
	if (ret < 0)
	{
		psvDebugScreenSetFgColor(COLOR_RED);
		psvDebugScreenPrintf("Error during initialization: cannot peek ctrl data (0x%08x)", ret);
		while (true) {}
	}

	redrawMenu(false);

	while (true)
	{
		SceCtrlData data;
		ret = sceCtrlPeekBufferPositive(0, &data, 1);
		if (ret < 0)
		{
			psvDebugScreenClear(COLOR_BLACK);
			psvDebugScreenSetFgColor(COLOR_RED);
			psvDebugScreenPrintf("Error: cannot peek ctrl data (0x%08x)", ret);
			while (true) {}
		}

		if (currentScreen == 0)
		{
			if ((data.buttons & SCE_CTRL_DOWN) && !(lastData.buttons & SCE_CTRL_DOWN))
			{
				transferMode ^= 1;
				redrawMenu(false);
			}
			if ((data.buttons & SCE_CTRL_UP) && !(lastData.buttons & SCE_CTRL_UP))
			{
				transferMode ^= 1;
				redrawMenu(false);
			}
			if ((data.buttons & confirmButton) && !(lastData.buttons & confirmButton))
			{
				selectedApp = 0;
				currentScreen = 1;
				scanApps(transferMode == 1);
				redrawMenu(true);
			}
		}
		else if (currentScreen == 1)
		{
			if ((data.buttons & SCE_CTRL_DOWN) && !(lastData.buttons & SCE_CTRL_DOWN))
			{
				selectedApp++;
				if (selectedApp >= currentApps.size())
					selectedApp = 0;
				redrawMenu(false);
			}
			if ((data.buttons & SCE_CTRL_UP) && !(lastData.buttons & SCE_CTRL_UP))
			{
				selectedApp--;
				if (selectedApp < 0)
					selectedApp = currentApps.size() - 1;
				redrawMenu(false);
			}
			if ((data.buttons & cancelButton) && !(lastData.buttons & cancelButton))
			{
				currentScreen = 0;
				redrawMenu(true);
			}
			if (currentApps.size() > 0 && (data.buttons & confirmButton) && !(lastData.buttons & confirmButton))
			{
				moveApp(transferMode == 0, currentApps[selectedApp]);
				scanApps(transferMode == 1);
				redrawMenu(true);
				continue;
			}
		}
		lastData = data;
	}

	sceKernelExitProcess(0);
	return 0;
}

void redrawMenu(bool clear)
{
	if (clear)
	{
		psvDebugScreenClear(COLOR_BLACK);
	}
	else
	{
		psvDebugScreenSetX(0);
		psvDebugScreenSetY(0);
	}
	psvDebugScreenSetFgColor(COLOR_CYAN);
	psvDebugScreenPrintf("Application Storage Manager v%s by Lupo511\n\n", VERSION);
	psvDebugScreenSetFgColor(COLOR_WHITE);
	psvDebugScreenPrintf("Press %s to select, %s to go back\n-------------------------------\n", confirmButton == SCE_CTRL_CROSS ? "X" : "O", cancelButton == SCE_CTRL_CIRCLE ? "O" : "X");
	if (currentScreen == 0)
	{
		psvDebugScreenSetFgColor(transferMode == 0 ? COLOR_YELLOW : COLOR_WHITE);
		psvDebugScreenPrintf("%sMemory Card -> Internal Storage\n", transferMode == 0 ? ">" : " ");
		psvDebugScreenSetFgColor(transferMode == 1 ? COLOR_YELLOW : COLOR_WHITE);
		psvDebugScreenPrintf("%sInternal Storage -> Memory Card\n", transferMode == 1 ? ">" : " ");
	}
	else if (currentScreen == 1)
	{
		if (currentApps.size() == 0)
		{
			psvDebugScreenSetFgColor(COLOR_WHITE);
			psvDebugScreenPrintf("<No games found>");
		}

		for (int i = 0; i < currentApps.size(); i++)
		{
			psvDebugScreenSetFgColor(selectedApp == i ? COLOR_YELLOW : COLOR_WHITE);
			psvDebugScreenPrintf("%s[%s] %s\n", selectedApp == i ? ">" : " ", currentApps[i].titleId.c_str(), currentApps[i].title.c_str());
		}
	}
}

void scanApps(bool internalStorage)
{
	currentApps.clear();

	sqlite3* db;
	int ret = sqlite3_open("ur0:shell/db/app.db", &db);
	if (ret < 0)
	{
		psvDebugScreenClear(COLOR_BLACK);
		psvDebugScreenSetFgColor(COLOR_RED);
		psvDebugScreenPrintf("Error: cannot open app.db (0x%08x)", ret);
		sqlite3_close(db);
		while (true) {}
	}

	vector<AppInfo> apps;

	string dev = internalStorage ? "ur0:" : "ux0:";
	string cmd = "select titleId, val from tbl_appinfo where key='2593862978' and val like '" + dev + "%'";
	char* errorMsg;
	ret = sqlite3_exec(db, cmd.c_str(), sqlAddAppCallback, &apps, &errorMsg);
	if (ret != SQLITE_OK)
	{
		psvDebugScreenClear(COLOR_BLACK);
		psvDebugScreenSetFgColor(COLOR_RED);
		psvDebugScreenPrintf("Error: cannot send tbl_appinfo command to app.db (0x%08x) (message: %s)", ret, errorMsg);
		sqlite3_close(db);
		sqlite3_free(errorMsg);
		while (true) {}
	}

	cmd = "select title, titleId from tbl_appinfo_icon order by title";
	ret = sqlite3_exec(db, cmd.c_str(), sqlSetTitleCallback, &apps, &errorMsg);
	if (ret != SQLITE_OK)
	{
		psvDebugScreenClear(COLOR_BLACK);
		psvDebugScreenSetFgColor(COLOR_RED);
		psvDebugScreenPrintf("Error: cannot send tbl_appinfo_icon command to app.db (0x%08x) (message: %s)", ret, errorMsg);
		sqlite3_close(db);
		sqlite3_free(errorMsg);
		while (true) {}
	}

	ret = sqlite3_close(db);
	if (ret != SQLITE_OK)
	{
		psvDebugScreenClear(COLOR_BLACK);
		psvDebugScreenSetFgColor(COLOR_RED);
		psvDebugScreenPrintf("Error: cannot close app.db (0x%08x)", ret);
		while (true) {}
	}
}

int sqlAddAppCallback(void* data, int argc, char** argv, char** cols)
{
	vector<AppInfo>* apps = (vector<AppInfo>*)data;
	AppInfo app;
	app.titleId = argv[0];
	apps->push_back(app);
	return 0;
}

int sqlSetTitleCallback(void* data, int argc, char** argv, char** cols)
{
	if (argv[0] == NULL || argv[1] == NULL)
		return 0;

	vector<AppInfo>* apps = (vector<AppInfo>*)data;
	for (int i = 0; i < apps->size(); i++)
	{
		if (apps->at(i).titleId.compare(argv[1]) == 0)
		{
			apps->at(i).title = argv[0];
			currentApps.push_back(apps->at(i));
			break;
		}
	}
	return 0;
}

void moveApp(bool toInternal, AppInfo app)
{
	psvDebugScreenClear(COLOR_BLACK);
	psvDebugScreenSetFgColor(COLOR_CYAN);
	psvDebugScreenPrintf("Application Storage Manager v%s by Lupo511\n", VERSION);
	psvDebugScreenPrintf("\n");
	psvDebugScreenSetFgColor(COLOR_WHITE);

	string sourceDev = toInternal ? "ux0:" : "ur0:";
	string destDev = toInternal ? "ur0:" : "ux0:";

	SceIoDevInfo info;
	memset(&info, 0, sizeof(SceIoDevInfo));
	//int ret = sceAppMgrGetDevInfo((char*)destDev.c_str(), &max_size, &free_size);
	int ret = sceIoDevctl(destDev.c_str(), 0x3001, 0, 0, &info, sizeof(SceIoDevInfo));
	if (ret < 0)
	{
		psvDebugScreenSetFgColor(COLOR_RED);
		psvDebugScreenPrintf("Error: couldn't get storage free space (0x%08x)\n", ret);
		psvDebugScreenSetFgColor(COLOR_WHITE);
		psvDebugScreenPrintf("Press X or O to continue");
		waitConfirm();
		return;
	}

	int sizeRequired = getFolderSize(sourceDev + "app/" + app.titleId);
	if (sizeRequired < 0)
	{
		psvDebugScreenSetFgColor(COLOR_RED);
		psvDebugScreenPrintf("Error: couldn't get app size (0x%08x)\n", sizeRequired);
		psvDebugScreenSetFgColor(COLOR_WHITE);
		psvDebugScreenPrintf("Press X or O to continue");
		waitConfirm();
		return;
	}
	else if (sizeRequired > info.free_size)
	{
		psvDebugScreenSetFgColor(COLOR_RED);
		psvDebugScreenPrintf("Error: not enough free space in %s\n", toInternal ? "internal storage" : "memory card");
		psvDebugScreenSetFgColor(COLOR_WHITE);
		psvDebugScreenPrintf("Press X or O to continue");
		waitConfirm();
		return;
	}

	psvDebugScreenPrintf("%s will be moved to %s, do you want to continue?\n", app.title.c_str(), toInternal ? "internal storage" : "memory card");
	psvDebugScreenPrintf("Press %s to confirm, %s to cancel\n", confirmButton == SCE_CTRL_CROSS ? "X" : "O", cancelButton == SCE_CTRL_CIRCLE ? "O" : "X");
	if (!waitConfirm())
		return;

	psvDebugScreenPrintf("Copying app files to the new storage...\n");
	//Just blindly try to create ur0:app if it doesn't exist for now
	if (toInternal)
	{
		ret = sceIoMkdir((destDev + "app").c_str(), 0777);
		if (ret < 0 && ret != SCE_ERROR_ERRNO_EEXIST)
		{
			psvDebugScreenSetFgColor(COLOR_YELLOW);
			psvDebugScreenPrintf("Warning: Error while trying to create ur0:app (0x%08x)\n", ret);
			psvDebugScreenSetFgColor(COLOR_WHITE);
		}
	}
	ret = copyFolder(sourceDev + "app/" + app.titleId, destDev + "app/" + app.titleId);
	if (ret < 0)
	{
		psvDebugScreenSetFgColor(COLOR_RED);
		psvDebugScreenPrintf("Error: coudldn't copy files (0x%08x)\n", ret);
		psvDebugScreenSetFgColor(COLOR_WHITE);
		psvDebugScreenPrintf("Press X or O to continue");
		waitConfirm();
		return;
	}

	psvDebugScreenPrintf("Editing app.db...\n");
	sqlite3* db;
	ret = sqlite3_open("ur0:shell/db/app.db", &db);
	if (ret != SQLITE_OK)
	{
		sqlite3_close(db);
		psvDebugScreenSetFgColor(COLOR_RED);
		psvDebugScreenPrintf("Error: cannot open app.db (0x%08x)\n", ret);
		psvDebugScreenSetFgColor(COLOR_WHITE);
		psvDebugScreenPrintf("Press X or O to continue");
		waitConfirm();
		return;
	}

	string cmd = "update tbl_appinfo set val=replace(val, '" + sourceDev + "', '" + destDev + "') where val like '" + sourceDev + "app/" + app.titleId + "%'";
	char* errMsg;
	ret = sqlite3_exec(db, cmd.c_str(), NULL, NULL, &errMsg);
	if (ret != SQLITE_OK)
	{
		sqlite3_close(db);
		psvDebugScreenSetFgColor(COLOR_RED);
		psvDebugScreenPrintf("Error: cannot send tbl_appinfo command to app.db (0x%08x) (%s)\n", ret, errMsg);
		psvDebugScreenSetFgColor(COLOR_WHITE);
		psvDebugScreenPrintf("Press X or O to continue");
		sqlite3_free(errMsg);
		waitConfirm();
		return;
	}

	cmd = "update tbl_appinfo_icon set iconPath=replace(iconPath, '" + sourceDev + "', '" + destDev + "') where iconPath like '" + sourceDev + "app/" + app.titleId + "%'";
	ret = sqlite3_exec(db, cmd.c_str(), NULL, NULL, &errMsg);
	if (ret != SQLITE_OK)
	{
		sqlite3_close(db);
		psvDebugScreenSetFgColor(COLOR_RED);
		psvDebugScreenPrintf("Error: cannot send tbl_appinfo_icon command to app.db (0x%08x) (%s)\n", ret, errMsg);
		psvDebugScreenSetFgColor(COLOR_WHITE);
		psvDebugScreenPrintf("Press X or O to continue");
		sqlite3_free(errMsg);
		waitConfirm();
		return;
	}

	ret = sqlite3_close(db);
	if (ret != SQLITE_OK)
	{
		psvDebugScreenSetFgColor(COLOR_RED);
		psvDebugScreenPrintf("Error: cannot close app.db (0x%08x)\n", ret);
		psvDebugScreenSetFgColor(COLOR_WHITE);
		psvDebugScreenPrintf("Press X or O to continue");
		waitConfirm();
		return;
	}

	psvDebugScreenPrintf("Deleting old files...\n");
	ret = removeFolder(sourceDev + "app/" + app.titleId);
	if (ret < 0)
	{
		psvDebugScreenSetFgColor(COLOR_RED);
		psvDebugScreenPrintf("Error: cannot delete directory %s (0x%08x)\nYou may delete it manually after the process finishes\n", (sourceDev + "app/" + app.titleId).c_str(), ret);
		psvDebugScreenSetFgColor(COLOR_WHITE);
	}

	psvDebugScreenSetFgColor(COLOR_GREEN);
	psvDebugScreenPrintf("%s was succesfully moved to %s!\n", app.title.c_str(), toInternal ? "internal storage" : "memory card");
	psvDebugScreenSetFgColor(COLOR_WHITE);
	psvDebugScreenPrintf("Press X or O to continue");
	waitConfirm();
}

int getFolderSize(string path)
{
	int size = 0;
	SceUID dir;
	dir = sceIoDopen(path.c_str());
	if (dir < 0)
	{
		return dir;
	}
	while (true)
	{
		SceIoDirent dirent;
		int ret = sceIoDread(dir, &dirent);
		if (ret == 0)
			break;
		else if (ret < 0)
			return ret;
		if (SCE_S_ISDIR(dirent.d_stat.st_mode))
		{
			ret = getFolderSize(path + "/" + dirent.d_name);
			if (ret < 0)
				return ret;
			size += ret;
		}
		else
		{
			size += dirent.d_stat.st_size;
		}
	}
	int ret = sceIoDclose(dir);
	if (ret < 0)
		return ret;
	return size;
}

int removeFolder(string path)
{
	SceUID dir = sceIoDopen(path.c_str());
	if (dir < 0)
		return dir;

	int ret = 0;
	SceIoDirent dirent;
	while (true)
	{
		ret = sceIoDread(dir, &dirent);
		if (ret < 0)
		{
			sceIoDclose(dir);
			return ret;
		}
		else if (ret == 0)
			break;
		if (SCE_S_ISDIR(dirent.d_stat.st_mode))
		{
			ret = removeFolder(path + "/" + dirent.d_name);
			if (ret < 0)
			{
				sceIoDclose(dir);
				return ret;
			}
		}
		else
		{
			ret = sceIoRemove((path + "/" + dirent.d_name).c_str());
			if (ret < 0)
			{
				sceIoDclose(dir);
				return ret;
			}
		}
	}

	ret = sceIoDclose(dir);
	if (ret < 0)
		return ret;

	ret = sceIoRmdir(path.c_str());
	if (ret < 0)
		return ret;

	return 0;
}

int copyFolder(string source, string destination)
{
	int ret = sceIoMkdir(destination.c_str(), 0777);
	if (ret < 0 && ret != SCE_ERROR_ERRNO_EEXIST)
	{
		return ret;
	}

	SceUID src = sceIoDopen(source.c_str());
	if (src < 0)
		return src;

	SceIoDirent dirent;
	while (true)
	{
		ret = sceIoDread(src, &dirent);
		if (ret == 0)
			break;
		else if (ret < 0)
		{
			sceIoDclose(src);
			return ret;
		}

		if (SCE_S_ISDIR(dirent.d_stat.st_mode))
		{
			ret = copyFolder(source + "/" + dirent.d_name, destination + "/" + dirent.d_name);
			if (ret < 0)
			{
				sceIoDclose(src);
				return ret;
			}
		}
		else
		{
			ret = copyFile(source + "/" + dirent.d_name, destination + "/" + dirent.d_name);
			if (ret < 0)
			{
				sceIoDclose(src);
				return ret;
			}
		}
	}
	sceIoDclose(src);
	return 0;
}

int copyFile(string source, string destination)
{
	SceUID src = sceIoOpen(source.c_str(), SCE_O_RDONLY, 0777);
	if (src < 0)
		return src;
	SceUID dest = sceIoOpen(destination.c_str(), SCE_O_WRONLY | SCE_O_CREAT, 0777);
	if (src < 0)
		return src;
	sceIoLseek(src, 0, SCE_SEEK_SET);
	sceIoLseek(dest, 0, SCE_SEEK_SET);

	void* data = malloc(TRANSFER_SIZE);
	while (true)
	{
		int bytesRead = sceIoRead(src, data, TRANSFER_SIZE);
		if (bytesRead < 0)
		{
			psvDebugScreenPrintf("Error while reading source file %s\n", source.c_str());
			free(data);
			sceIoClose(src);
			sceIoClose(dest);
			return bytesRead;
		}
		else if (bytesRead == 0)
		{
			break;
		}

		bytesRead = sceIoWrite(dest, data, bytesRead);
		if (bytesRead < 0)
		{
			psvDebugScreenPrintf("Error while reading destination file %s\n", source.c_str());
			free(data);
			sceIoClose(src);
			sceIoClose(dest);
			return bytesRead;
		}
	}
	free(data);
	sceIoClose(src);
	sceIoClose(dest);
	return 0;
}

bool waitConfirm()
{
	int ret = sceCtrlPeekBufferPositive(0, &lastData, 1);
	if (ret < 0)
	{
		psvDebugScreenSetFgColor(COLOR_RED);
		psvDebugScreenPrintf("Error : cannot peek ctrl data (0x%08x)", ret);
		while (true) {}
	}
	while (true)
	{
		SceCtrlData data;
		ret = sceCtrlPeekBufferPositive(0, &data, 1);
		if (ret < 0)
		{
			psvDebugScreenSetFgColor(COLOR_RED);
			psvDebugScreenPrintf("Error : cannot peek ctrl data (0x%08x)", ret);
			while (true) {}
		}
		if ((data.buttons & cancelButton) && !(lastData.buttons & cancelButton))
		{
			lastData = data;
			return false;
		}
		if ((data.buttons & confirmButton) && !(lastData.buttons & confirmButton))
		{
			lastData = data;
			return true;
		}
		lastData = data;
	}
}

void getConfirmButton()
{
	int ret;
	if (sceRegMgrGetKeyInt("/CONFIG/SYSTEM", "button_assign", &ret) < 0 || ret == 1)
	{
		confirmButton = SCE_CTRL_CROSS;
		cancelButton = SCE_CTRL_CIRCLE;
	}
	else
	{
		confirmButton = SCE_CTRL_CIRCLE;
		cancelButton = SCE_CTRL_CROSS;
	}
}