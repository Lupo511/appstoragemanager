#pragma once
#include <string.h>
#include <psp2/types.h>
#include <psp2/apputil.h>
#include <psp2/kernel/clib.h>
#include <psp2/common_dialog.h>

#ifdef __cplusplus
extern "C" {
#endif
	typedef enum SceSaveDataDialogMode {
		SCE_SAVEDATA_DIALOG_MODE_FIXED = 1,
		SCE_SAVEDATA_DIALOG_MODE_LIST = 2,
		SCE_SAVEDATA_DIALOG_MODE_USER_MSG = 3,
		SCE_SAVEDATA_DIALOG_MODE_SYSTEM_MSG = 4,
		SCE_SAVEDATA_DIALOG_MODE_ERROR_CODE = 5,
		SCE_SAVEDATA_DIALOG_MODE_PROGRESS_BAR = 6
	} SceSaveDataDialogMode;

	typedef enum SceSaveDataDialogType {
		SCE_SAVEDATA_DIALOG_TYPE_SAVE = 1,
		SCE_SAVEDATA_DIALOG_TYPE_LOAD = 2
	} SceSaveDataDialogType;

	typedef enum SceSaveDataDialogEnvFlag {
		SCE_SAVEDATA_DIALOG_ENV_FLAG_DEFAULT = 0
	} SceSaveDataDialogEnvFlag;

	typedef enum SceSaveDataDialogFocusPos {
		SCE_SAVEDATA_DIALOG_FOCUS_POS_LISTHEAD = 0
	} SceSaveDataDialogFocusPos;

	typedef unsigned int SceAppUtilSaveSlotId;

	typedef enum SceSaveDataDialogButtonType {
		SCE_SAVEDATA_DIALOG_BUTTON_TYPE_OK = 0,
		SCE_SAVEDATA_DIALOG_BUTTON_TYPE_YESNO = 1,
		SCE_SAVEDATA_DIALOG_BUTTON_TYPE_NONE = 2
	} SceSaveDataDialogButtonType;

	typedef enum SceSaveDataDialogSystemMessageType {
		SCE_SAVEDATA_DIALOG_SYSMSG_TYPE_NODATA = 1,
		SCE_SAVEDATA_DIALOG_SYSMSG_TYPE_CONFIRM = 2,
		SCE_SAVEDATA_DIALOG_SYSMSG_TYPE_OVERWRITE = 3,
		SCE_SAVEDATA_DIALOG_SYSMSG_TYPE_NOSPACE = 4,
		SCE_SAVEDATA_DIALOG_SYSMSG_TYPE_PROGRESS = 5,
		SCE_SAVEDATA_DIALOG_SYSMSG_TYPE_FINISHED = 6
	} SceSaveDataDialogSystemMessageType;

	typedef enum SceSaveDataDialogProgressBarType {
		SCE_SAVEDATA_DIALOG_PROGRESSBAR_TYPE_PERCENTAGE = 0
	} SceSaveDataDialogProgressBarType;

	typedef struct SceSaveDataDialogFixedParam {
		SceAppUtilSaveDataSlot targetSlot;
		SceChar8 reserved[32];
	} SceSaveDataDialogFixedParam;

	typedef struct SceSaveDataDialogListParam {
		const SceAppUtilSaveDataSlot *slotList;
		SceUInt slotListSize;
		SceSaveDataDialogFocusPos focusPos;
		SceAppUtilSaveSlotId focusId;
		const SceChar8 *listTitle;
		SceChar8 reserved[32];
	} SceSaveDataDialogListParam;

	typedef struct SceSaveDataDialogUserMessageParam {
		SceSaveDataDialogButtonType buttonType;
		const SceChar8 *msg;
		SceAppUtilSaveDataSlot targetSlot;
		SceChar8 reserved[32];
	} SceSaveDataDialogUserMessageParam;

	typedef struct SceSaveDataDialogSystemMessageParam {
		SceSaveDataDialogSystemMessageType sysMsgType;
		SceInt32 value;
		SceAppUtilSaveDataSlot targetSlot;
		SceChar8 reserved[32];
	} SceSaveDataDialogSystemMessageParam;

	typedef struct SceSaveDataDialogErrorCodeParam {
		SceInt32 errorCode;
		SceAppUtilSaveDataSlot targetSlot;
		SceChar8 reserved[32];
	} SceSaveDataDialogErrorCodeParam;

	typedef struct SceSaveDataDialogProgressBarParam {
		SceSaveDataDialogProgressBarType barType;
		SceSaveDataDialogSystemMessageParam sysMsgParam;
		const SceChar8 *msg;
		SceAppUtilSaveDataSlot targetSlot;
		SceChar8 reserved[32];
	} SceSaveDataDialogProgressBarParam;

	typedef struct SceSaveDataDialogSlotConfigParam {
		const SceAppUtilSaveDataMountPoint *mountPoint;
		const SceChar8 *appSubDir;
		SceChar8 reserved[32];
	} SceSaveDataDialogSlotConfigParam;

	typedef struct SceSaveDataDialogParam {
		SceUInt32 sdkVersion;
		SceCommonDialogParam commonParam;
		SceSaveDataDialogMode mode;
		SceSaveDataDialogType dispType;
		SceSaveDataDialogFixedParam *fixedParam;
		SceSaveDataDialogListParam *listParam;
		SceSaveDataDialogUserMessageParam *userMsgParam;
		SceSaveDataDialogSystemMessageParam *sysMsgParam;
		SceSaveDataDialogErrorCodeParam *errorCodeParam;
		SceSaveDataDialogProgressBarParam *progBarParam;
		SceSaveDataDialogSlotConfigParam *slotConfParam;
		SceSaveDataDialogEnvFlag flag;
		SceChar8 reserved[32];
	} SceSaveDataDialogParam;

	typedef enum SceSaveDataDialogButtonId {
		SCE_SAVEDATA_DIALOG_BUTTON_ID_INVALID = 0,
		SCE_SAVEDATA_DIALOG_BUTTON_ID_OK = 1,
		SCE_SAVEDATA_DIALOG_BUTTON_ID_YES = 1,
		SCE_SAVEDATA_DIALOG_BUTTON_ID_NO = 2
	} SceSaveDataDIalogButtonId;

	typedef struct SceSaveDataDialogSlotInfo {
		SceUInt32 isExist;
		SceAppUtilSaveDataSlotParam *slotParam;
		SceUInt8 reserved[32];
	} SceSaveDataDialogSlotInfo;

	typedef struct SceSaveDataDialogResult {
		SceSaveDataDialogMode mode;
		SceInt32 result;
		SceSaveDataDialogButtonId buttonId;
		SceAppUtilSaveDataSlotId slotId;
		SceSaveDataDialogSlotInfo *slotInfo;
		void *userdata;
		SceUInt8 reserved[32];
	} SceSaveDataDialogResult;

	static inline void sceSaveDataDialogParamInit(SceSaveDataDialogParam *param) {
		memset(param, 0x0, sizeof(SceSaveDataDialogParam));
		param->sdkVersion = 0x03150021;
		_sceCommonDialogSetMagicNumber(&param->commonParam);
	}
	SceInt32 sceSaveDataDialogInit(const SceSaveDataDialogParam *param);
	SceCommonDialogStatus sceSaveDataDialogGetStatus();
	SceCommonDialogStatus sceSaveDataDialogGetSubStatus();
	SceInt32 sceSaveDataDialogGetResult(SceSaveDataDialogResult *result);
#ifdef __cplusplus
}
#endif