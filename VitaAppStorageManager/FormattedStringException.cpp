#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include "FormattedStringException.h"

FormattedStringException::FormattedStringException(const char* format, ...) throw()
{
	char* str;
	va_list args;
	va_start(args, format);
	vasprintf(&str, format, args);
	va_end(args);
	msg = "";
	msg.append(str);
	free(str);
}

const char * FormattedStringException::what() const  throw()
{
	return msg.c_str();
}

FormattedStringException::~FormattedStringException() throw()
{
}
